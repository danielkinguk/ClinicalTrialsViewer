<?php

if ($_SERVER["HTTPS"] == "on") {

    $siteurl = "https://" . $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

} else {

    $siteurl = "http://" . $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

}

$abspath = substr (__DIR__, 0, strlen ( __DIR__ ) - 7);

?><!DOCTYPE html>
<html lang="en">

    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<title>Clinical trials viewer installer</title>

	<!-- Bootstrap -->
	<link href="<?php echo $siteurl; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- CTNotebook CSS -->
	<link href="<?php echo $siteurl; ?>ctviewer.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>

	<div class="container-fluid ctviewer-nav" id="nav-container" style="position: fixed; top: 0; z-index: 1000;">
	    <ul class="nav">
		<li class="nav-item">
		    <a class="nav-link active" href="<?php echo $siteurl; ?>">Clinical trials viewer installer</a>
		</li>
	    </ul>
	</div>

	<div class="container" style="margin-top: 50px;">
	    <div class="row">
		<div class="col-md-8 offset-md-2">

		    <div id="ctvInstallPane">

			<h2>MySQL details</h2>
			<div class="form-group">
			    <label for="ctvDBusername">Database username</label>
			    <input type="text" class="form-control" id="ctvDBusername" placeholder="Database username">
			</div>
			
			<div class="form-group">
			    <label for="ctvDBpassword">Database password</label>
			    <input type="text" class="form-control" id="ctvDBpassword" placeholder="Database password">
			</div>

			<div class="form-group">
			    <label for="ctvDBname">Database name</label>
			    <input type="text" class="form-control" id="ctvDBname" placeholder="Database name">
			</div>
			
			<div class="form-group">
			    <label for="ctvDBhost">Database host</label>
			    <input type="text" class="form-control" id="ctvDBhost" placeholder="Database host" aria-described-by="ctvDBhost-help">
			    <small id="ctvDBhost-help" class="form-text text-muted">E.g. "localhost"</small>
			</div>
			
			<button class="btn btn-primary btn-block" onclick="ctvTestMySQLConnexion();">Test database connexion</button>
			
			<div id="ctvDBTestFeedback" class="ctvHidden alert" role="alert" style="margin-top: 10px;"></div>
			
			<h2 style="margin-top: 10px;">Site details</h2>
			<div class="alert alert-warning" role="alert">The following should be detected automatically; only change if you know what you're doing.</div>

			<div class="form-group">
			    <label for="ctvAbsPath">Absolute path to installation</label>
			    <input type="text" class="form-control" id="ctvAbsPath" placeholder="Absolute path" value="<?php echo $abspath; ?>">
			</div>

			<div class="form-group">
			    <label for="ctvAbsPath">Site URL</label>
			    <input type="text" class="form-control" id="ctvSiteURL" placeholder="Site URL" value="<?php echo $siteurl; ?>">
			</div>

			<button class="btn btn-block btn-primary" onclick="ctvWriteConfig();">Install</button>
		    </div>
		    
		</div>
	    </div>
	</div>

	
	
	<!-- jQuery -->
	<script src="<?php echo $siteurl; ?>jquery-3.4.1.min.js"></script>

	<!-- CTViewer JS -->
	<script type="text/javascript">

	 function ctvTestMySQLConnexion () {

	     $.ajax ({
		 url: '<?php echo $siteurl; ?>install/testmysql.php',
		 type: 'post',
		 data: {
		     dbusername: $('#ctvDBusername').val(),
		     dbpassword: $('#ctvDBpassword').val(),
		     dbname: $('#ctvDBname').val(),
		     dbhost: $('#ctvDBhost').val(),
		 },
		 dataType: 'html'
	     }).done (function (response) {
		 $('#ctvDBTestFeedback').html(response);

		 if ( response == 'Successful connexion to MySQL database') {

		     $('#ctvDBTestFeedback').removeClass('alert-danger').addClass('alert-success');
		     
		 } else {

		     $('#ctvDBTestFeedback').removeClass('alert-success').addClass('alert-danger');
		     
		 }

		 $('#ctvDBTestFeedback').slideDown( 500, function () {

		     setTimeout ( function () {

			 $('#ctvDBTestFeedback').slideUp(500);
			 
		     }, 3000 );
		     
		 });
	     });
	     
	 }

	 function ctvWriteConfig () {

	     $.ajax ({
		 url: '<?php echo $siteurl; ?>install/writeconfig.php',
		 type: 'post',
		 data: {
		     dbusername: $('#ctvDBusername').val(),
		     dbpassword: $('#ctvDBpassword').val(),
		     dbname: $('#ctvDBname').val(),
		     dbhost: $('#ctvDBhost').val(),
		     abs_path: $('#ctvAbsPath').val(),
		     site_url: $('#ctvSiteURL').val(),
		 },
		 datatype: 'html'
	     }).done ( function ( response ) {
		 $('#ctvInstallPane').html(response);
	     });
	 }
	</script>
	<!-- / CTViewer JS -->
    </body>
</html>
