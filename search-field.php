<div class="container" style="margin-top: 50px;">
    <div class="row">
	<div class="col-md-8 offset-md-2">
	    <div id="search-form">
		<h1>Search for clinical trials and FDA info</h1>
		
		<div class="form-group">
		    <label for="drug-query">Enter an intervention (drug; medical device name)</label>
		    <div class="input-group">
			<input type="text" class="form-control" id="drug-query" aria-describedby="drug-query-help" placeholder="Intervention" autofocus>
			<div class="input-group-append">
			    <button type="button" class="btn btn-primary" onclick="ctviewer_search();">Search</button>
			</div>
		    </div>
		    <small id="drug-query-help" class="form-text text-muted">E.g. "sunitinib," "pembrolizumab," etc.</small>
		</div>
		<div class="card ctvHidden" id="ctvAdvancedSearchOptions" style="margin-top: 20px;">
		    <h2 class="card-header">Advanced search options</h2>
		    <div class="card-body">
			<div class="form-group">
			    <label for="indication-query">Narrow results by indication</label>
			    <input type="text" class="form-control" id="indication-query" aria-describedby="indication-help" placeholder="Indication">
			    <small id="indication-help" class="form-text text-muted">E.g. "breast cancer"; leave this field blank to search for any indication</small>
			</div>
			<div class="form-group form-check">
			    <input type="checkbox" class="form-check-input" id="paeds-checkbox">
			    <label class="form-check-label" for="paeds-checkbox">Search paediatric only</label>
			</div>
			<button class="btn btn-primary btn-sm" onclick="$('#ctvAdvancedSearchOptions').slideUp();$('#ctvAdvancedSearchButton').slideDown();$('#indication-query').val('');$('#paeds-checkbox').prop('checked', false);">Close advanced search options</button>
		    </div>
		</div>
		<button class="btn btn-primary btn-sm" id="ctvAdvancedSearchButton" onclick="$('#ctvAdvancedSearchOptions').slideDown();$(this).slideUp(0);">Show advanced search options</button>		
	    </div>
	    <div id="search-progress" style="display: none;"></div>
	</div>
    </div>
</div>
