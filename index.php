<?php

if ( ! file_exists ( __DIR__ . "/install/" ) ) { // Has been installed

    include_once ('config.php');

    include ( ABS_PATH . 'page.php');
    
} else { // Needs to run the installer

    include ('install/install.php');
}

